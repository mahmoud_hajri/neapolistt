import sys
import inspect
import ctypes


class log_content(object):
    
    def __init__(self, func):
        
        self._locals = {}
        self.func = func
        
        
    def __call__(self, *args, **kwargs):
       
        
        def tracer(frame, event, arg):
            
            
               
                self._locals = frame.f_locals.copy()
                
                
                keys, values = zip(*self._locals.items())
                
                
               
                

                if(event!='return'):
                    # print(self._locals)
                    for k in range (0,len(keys)):
                       
                       
                        frame.f_locals.update({
                                        keys[k] : values[k]
                                        
                                        
                                                    })
                            
                        ctypes.pythonapi.PyFrame_LocalsToFast(ctypes.py_object(frame), ctypes.c_int(0))
                    self._locals = frame.f_locals.copy()
              
            
             
                # print("this is ",frame.f_locals)
                
                if event=='return':
               
                
                
                    
                    keyslist=list(self._locals.keys())
                    
                    allvariables=(self._locals)
                    Largs= list(args)
                    # print("Largs ",Largs)
                    argspec = str(inspect.signature(self.func))
                    # print("argspec  ",argspec )
                    s1=argspec.replace('(','')
                    s1=s1.replace(')','')
                    s1=s1.replace(' ','')
                    # print("s1 ",s1)
                    argspeclist=s1.split(",")
                    # print("argspeclist", argspeclist )
                    
                    
                   
                    
                    m=max(len(argspeclist),len(Largs))
                    d={}
                    for i in range(0,m):
                        if (keyslist[i]==argspeclist[i]):
                           
                            d[argspeclist[i]] = Largs[i]
                    
                  
                        
                    for x in list(allvariables):
                        
                        for xx in list(d):
                           
                            if xx == x:
                                allvariables.pop(x)
                    
                  
                    
                    
                    
                    for final in d:
                        print("variable",final,"=",d[final])
                        
                        
                    
                    for final in allvariables:
                        print("variable",final,"= ",allvariables[final])
                   
                    
            
       
        sys.setprofile(tracer)
        
        
       
        try:
              
                res = self.func(*args, **kwargs)
                
        finally:
            
            sys.setprofile(None)
        return res
    
    

@log_content
def example(a,b,c):
    a = b + c
    b = a + c
    c = a + b
    d = "hi there"
   
    
    result = a + b + c
    return result

example(5, 4, 3)





